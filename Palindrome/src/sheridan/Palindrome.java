package sheridan;

public class Palindrome {

	public static boolean isPalindrome(String input) {
		input=input.toLowerCase().replaceAll(" ","");
		for(int i=0,j=input.length()-1;i<j;i++,j--) {
			if(input.charAt(i)!=input.charAt(j)) {
				return false;
			}
		}
	 return true;
	}

	public static void main(String[] args) {
		System.out.println("is anna palindrome?"+Palindrome.isPalindrome("anna"));
		System.out.println("is dog palindrome?"+Palindrome.isPalindrome("dog"));
	}
}
