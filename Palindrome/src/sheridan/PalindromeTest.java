package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
	assertTrue("Invalid Input",Palindrome.isPalindrome("anna"));
	}
	@Test
	public void testIsNotPalindrome() {
	assertFalse("Invalid Input",Palindrome.isPalindrome("dog"));
	}
	@Test
	public void testIsBorder1Palindrome() {
	assertFalse("Invalid Input",Palindrome.isPalindrome("race ca"));
	}
	@Test
	public void testIsBorder2Palindrome() {
	assertTrue("Invalid Input",Palindrome.isPalindrome("race      car"));
	}
	

}
